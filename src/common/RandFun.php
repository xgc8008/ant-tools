<?php

namespace AntStudio\AntToolsSdk\common;
/**
 * 随机处理类
 */
class RandFun
{
    /**
     *  随机数-大小写字母+数字
     * @param $length
     * @param $prefix
     * @return string
     */
    public function randByLengthUpper($length, $prefix = '')
    {
        $hash  = $prefix;
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $max   = strlen($chars) - 1;
        mt_srand((double)microtime() * 1000000);
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }

        return $hash;
    }

    /**
     *  随机数-小写字母+数字
     * @param $length
     * @param $prefix
     * @return mixed|string
     */
    public function randByLengthLower($length, $prefix = '')
    {
        $hash  = $prefix;
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $max   = strlen($chars) - 1;
        mt_srand((double)microtime() * 1000000);
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }

        return $hash;
    }

    /**
     * 随机数-纯数字
     * @param $length
     * @return string
     */
    public function randByLengthNum($length)
    {
        $hash  = '';
        $chars = '0123456789';
        $max   = strlen($chars) - 1;
        mt_srand((double)microtime() * 1000000);
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }

        return $hash;
    }
}
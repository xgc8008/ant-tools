<?php

namespace AntStudio\AntToolsSdk\common;
/**
 * 字答串类处理
 */
class StringFun
{
    /**
     * utf8mb4表情转换，用于存储
     * @param $str
     * @return mixed|string
     */
    function textEncode($str) {
        if (!is_string($str)) {
            return $str;
        }

        if (!$str || $str == 'undefined') {
            return '';
        }

        $text = json_encode($str); //暴露出unicode
        $text = preg_replace_callback("/(\\\u[ed][0-9a-f]{3})/i", function ($str) {
            return addslashes($str[0]);
        }, $text); //将emoji的unicode留下，其他不动，这里的正则比原答案增加了d，因为我发现我很多emoji实际上是\ud开头的，反而暂时没发现有\ue开头。
        return json_decode($text);
    }

    /**
     * 表情反转，用于显示
     * @param $str
     * @return mixed
     */
    function textDecode($str) {
        $text = json_encode($str); //暴露出unicode
        $text = preg_replace_callback('/\\\\\\\\/i', function ($str) {
            return '\\';
        }, $text); //将两条斜杠变成一条，其他不动
        return json_decode($text);
    }
}
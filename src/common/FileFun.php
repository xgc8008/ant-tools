<?php

namespace AntStudio\AntToolsSdk\common;
/**
 * 文件处理类
 */
class FileFun
{
    /**
     * 文件名后缀
     * @param $filename
     * @return false|string
     */
    public function fileExt($filename)
    {
        return substr(strrchr($filename, '.'), 1);
    }
}
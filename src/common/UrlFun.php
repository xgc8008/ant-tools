<?php

namespace AntStudio\AntToolsSdk\common;
/**
 * url地址处理
 */
class UrlFun
{
    /**
     * 1处理带有中文URL
     * @param $url
     * @return string
     */
    function urlencode_ch($url)
    {
        $uri = '';
        $cs  = unpack('C*', $url);
        $len = count($cs);
        for ($i = 1; $i <= $len; $i++) {
            $uri .= $cs[$i] > 127 ? '%' . strtoupper(dechex($cs[$i])) : $url{$i - 1};
        }

        return $uri;
    }

    /**
     * 2处理带有中文URL
     * @param $str
     * @return array|string|string[]|null
     */
    function urlencode_ch1($str)
    {
        //正则表达式匹配非单字节字符（含中文）
        function callback($match)
        {
            return urlencode($match[0]);
        }

        return preg_replace_callback('/[^\0-\127]+/', 'callback', $str);

    }

    /**
     * 格式化中文路径
     * @param $vdo_url
     * @return string
     */
    function formatChURL($vdo_url)
    {
        $vdo_url_arr = explode('/', $vdo_url);
        foreach ($vdo_url_arr as $key_it => &$value_it) {
            if (preg_match('/[\x7f-\xff]/', $value_it)) {
                //echo $value_it.'=>字符串中有中文<br/>';
                $value_it = urlencode($value_it);
            } else {
                //echo '字符串中没有中文<br/>';
            }
        }

        return implode('/', $vdo_url_arr);
    }
}
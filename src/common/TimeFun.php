<?php

namespace AntStudio\AntToolsSdk\common;
/**
 * 日期时间处理
 */
class TimeFun
{
    /**
     * 日期操作
     * @param $datetype 类型
     * @param $doaction 操作 + -
     * @param $donum 数值
     * @param $datetime 日期
     * @return false|int
     * $datetype day也可以改成year（年），month（月），hour（小时）minute（分），second（秒）
     */
    public function doDate($datetype, $doaction, $donum, $datetime = '')
    {
        if (empty($datetime)) {
            $datetime = time();
        }

        return strtotime($doaction . $donum . ' ' . $datetype, $datetime);
    }

    /**
     * 时间倒计时
     * @param $sTime 时间戳
     * @return string
     */
    function timeDjs($sTime) {
        // $startTime = '09:00:00'; $endTime = '18:00:00';   // 将时间转化为unix时间戳
        // $startTimeStr = strtotime($startTime);
        // $endTimeStr = strtotime($endTime);
        // $total = $endTimeStr - $startTimeStr;
        // $restHours = 1;         // 休息1小时
        // $now = strtotime(date('H:i:s'));
        // $remain = $endTimeStr - $now;
        $cTime = time();
        $dTime = intval($sTime) - $cTime;
        $secondtime = intval($sTime) - $cTime;
        $dDay = intval(date("z", $sTime)) - intval(date("z", $cTime));
        //$dDay     =   intval($dTime/3600/24);
        $dYear = intval(date("Y", $sTime)) - intval(date("Y", $cTime));
        $dmin = intval(date("i", $sTime)) - intval(date("i", $cTime));

        $time_str = '';

        $time_str .= $dDay . '天' . $dmin . '分' . floor(($dTime / 3600)) . '小时' . floor($dTime / 60) . '分钟';

        $second = $secondtime % 60; //取余得到秒数

        $nowtime = floor($secondtime / 60); //转化成分钟

        $minute = $nowtime % 60; //取余得到分钟数

        $nowtime = floor($nowtime / 60); //转化成小时

        $hour = $nowtime % 24; //取余得到小时数

        $nowtime = floor($nowtime / 24); //转化成天数

        //$day=floor($nowtime);//得到天数

        $time_str = ''; //$nowtime.'天 '.$hour.'小时'.$minute.'分'.$second.'秒';
        if ($nowtime > 0) {
            $time_str .= $nowtime . '天 '; //.$hour.'小时'.$minute.'分'.$second.'秒';
        }

        if ($hour > 0) {
            $time_str .= $hour . '小时'; //.$minute.'分'.$second.'秒';
        }

        if ($minute > 0) {
            $time_str .= $minute . '分';
        }

        if ($nowtime > 0 || $hour > 0 || $minute > 0 || $second > 0) {
            $time_str .= $second . '秒';
        }
        return $time_str;
    }

    /**
     * 秒转分钟
     * @param $num
     * @return string|void
     */
    function secDataFormat($num)
    {
        $hour   = floor($num / 3600);
        $minute = floor(($num - 3600 * $hour) / 60);
        $second = floor((($num - 3600 * $hour) - 60 * $minute) % 60);
        if ($num == 0) {
            return '00:' . $this->dispRepair($num, 2, '0');
        } else {
            if ($hour > 0) {
                return $this->dispRepair($hour, 2, '0') . ':' . $this->dispRepair($minute, 2, '0') . ':' . $this->dispRepair($second, 2, '0');
            } else if ($minute > 0) {
                return $this->dispRepair($minute, 2, '0') . ':' . $this->dispRepair($second, 2, '0');
            } else if ($second > 0) {
                return '00:' . $this->dispRepair($second, 2, '0');
            }
        }

    }
}